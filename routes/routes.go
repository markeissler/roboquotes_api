package routes

import (
	"bitbucket.org/markeissler/Roboquotes_API/controllers"
	"bitbucket.org/markeissler/Roboquotes_API/middleware"
	"github.com/gorilla/mux"
)

func Register() *mux.Router {
	router := mux.NewRouter()

	// insert middleware for setting standard headers
	router.Use(middleware.StandardHeaders)

	// handlers not requiring auth
	router.HandleFunc("/v1/", controllers.MainHandler).Methods("GET")
	router.HandleFunc("/v1/register", controllers.CreateUser).Methods("POST")
	router.HandleFunc("/v1/login", controllers.Login).Methods("POST")

	// handlers requiring auth
	subRouter := router.PathPrefix("/v1/auth/").Subrouter()
	subRouter.Use(middleware.ValidateToken)

	// users
	subRouter.HandleFunc("/users/{id}", controllers.FetchUser).Methods("GET")

	// quotes
	subRouter.HandleFunc("/quotes/random", controllers.FetchRandomQuote).Methods("GET")

	return router
}
