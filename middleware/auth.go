package middleware

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/markeissler/Roboquotes_API/controllers"
	"bitbucket.org/markeissler/Roboquotes_API/models"
	jwt "github.com/dgrijalva/jwt-go"
)

const AuthorizationSchema = "Bearer "

func ValidateToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// get token from header
		accessToken := r.Header.Get("Authorization")
		// remove leading/trailing spaces
		accessToken = strings.TrimSpace(accessToken)

		if accessToken == "" {
			err := controllers.ErrorResponse{
				Message: errors.New("token missing").Error(),
			}
			log.Println(err)
			w.WriteHeader(http.StatusForbidden)
			json.NewEncoder(w).Encode(err)
			return
		}

		// FIXME: Handle the case where "Authorization: Bearer " exists without a value.

		// remove the schema string
		accessToken = accessToken[len(AuthorizationSchema):]

		claims := new(models.Claims)

		_, err := jwt.ParseWithClaims(accessToken, claims, func(token *jwt.Token) (interface{}, error) {
			return []byte("secret"), nil
		})
		if err != nil {
			log.Println(err)
			err := controllers.ErrorResponse{
				Message: err.Error(),
			}
			w.WriteHeader(http.StatusForbidden)
			json.NewEncoder(w).Encode(err)
			return
		}

		context := context.WithValue(r.Context(), "user", claims)

		// call the next middleware with the authorized context
		next.ServeHTTP(w, r.WithContext(context))
	})
}
