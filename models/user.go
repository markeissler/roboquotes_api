package models

import (
	"errors"
	"fmt"
	"time"

	"golang.org/x/crypto/bcrypt"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
)

const jwtDefaultExpirySeconds = 3600

type Claims struct {
	jwt.StandardClaims
	UserID uint
	Name   string
	Email  string
}

type Response struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
	Token   string `json:"token"`
	User    User   `json:"user"`
}

type User struct {
	gorm.Model
	Name         string `gorm:"column:name" json:"name"`
	Email        string `gorm:"column:email;type:varchar(100);unique_index" json:"email"`
	Password     string `gorm:"column:password" json:"password"`
	RobohashSeed string `gorm:"column:robohash_seed" json:"robohash_seed"`
	RobohashUrl  string `gorm:"-" json:"robohash_url"`
}

func (u *User) Authenticate(db *gorm.DB, email, password string) (*Response, error) {
	// fetch user from db
	if err := db.Where("email = ?", email).First(u); err.Error != nil {
		fmt.Println(err.Error)
		return nil, errors.New(fmt.Sprintf("email address not found: %v", err.Error))
	}

	// compare passwords
	if err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password)); err != nil {
		fmt.Println(err)
		return nil, errors.New(fmt.Sprintf("email address not found: %v", err.Error()))
	}

	//
	// create jwt
	//
	expiresAt := time.Now().Add(time.Second * jwtDefaultExpirySeconds).Unix()

	tokenClaims := &Claims{
		UserID: u.ID,
		Name:   u.Name,
		Email:  u.Email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tokenClaims)
	if token == nil {
		err := errors.New("failed to generate new token")
		fmt.Println(err)
		return nil, err
	}

	tokenString, err := token.SignedString([]byte("secret"))
	if err != nil {
		fmt.Println(err)
		errors.New(fmt.Sprintf("token invalid: %v", err.Error()))
		return nil, err
	}

	response := &Response{
		Status:  true,
		Message: "logged in",
		Token:   tokenString,
		User:    *u,
	}

	return response, nil
}
