package models

import "github.com/jinzhu/gorm"

type Quote struct {
	gorm.Model
	Text    string `gorm:"column:text" json:"text"`
	Rating  int    `gorm:"column:rating" json:"rating"`
	Seed    string `gorm:"column:seed" json:"seed"`
	Slug    string `gorm:"column:slug" json:"slug"`
	Content string `gorm:"content:content" json:"content"`
	Title   string `gorm:"content:title" json:"title"`
}
