package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/markeissler/Roboquotes_API/models"
)

const (
	quotesOnDesignURLTemplate = `https://quotesondesign.com/wp-json/wp/v2/posts/?orderby=rand&_=%s&per_page=1`
	randMax                   = 1568851570149
)

type renderedField struct {
	Rendered  string `json:"rendered"`
	Protected bool   `json:"protected"`
}

type rawQuote struct {
	Slug    string        `json:"slug"`
	Content renderedField `json:"content"`
	Title   renderedField `json:"title"`
}

func QuotesOnDesignGetRandom() (*models.Quote, error) {
	rand.Seed(time.Now().UnixNano())
	randomNumber := rand.Intn(randMax)

	url := fmt.Sprintf(quotesOnDesignURLTemplate, strconv.Itoa(randomNumber))

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	client := http.Client{
		Timeout: 15 * time.Second,
	}

	req.Header.Set("User-Agent", "roboquote-bot")

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	rawQuotes := []rawQuote{}
	if err := json.Unmarshal(body, &rawQuotes); err != nil {
		return nil, err
	}

	if len(rawQuotes) == 0 {
		return nil, errors.New("no quotes returned")
	}

	quote := new(models.Quote)
	quote.Title = rawQuotes[0].Title.Rendered
	quote.Text = rawQuotes[0].Content.Rendered
	quote.Slug = rawQuotes[0].Slug

	return quote, nil
}
