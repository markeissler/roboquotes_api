package utils

import (
	"errors"
	"fmt"
	"os"

	"bitbucket.org/markeissler/Roboquotes_API/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/joho/godotenv"
)

func ConnectDB() (*gorm.DB, error) {
	// load env vars
	if err := godotenv.Load(); err != nil {
		return nil, errors.New("error loading env vars")
	}

	databaseUser := os.Getenv("databaseUser")
	databasePass := os.Getenv("databasePass")
	databaseName := os.Getenv("databaseName")
	databaseHost := os.Getenv("databaseHost")
	databasePort := os.Getenv("databasePort")
	databaseType := os.Getenv("databaseType")

	// db connection string
	dbURI := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		databaseHost, databasePort, databaseUser, databasePass, databaseName)

	// connect to db
	db, err := gorm.Open(databaseType, dbURI)
	if err != nil {
		return nil, err
	}

	// migrate the database
	db.AutoMigrate(&models.User{}, &models.Quote{})

	return db, nil
}
