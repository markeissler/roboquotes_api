package utils

import (
	"crypto/md5"
	"fmt"
)

const roboHashURLTemplate = `https://robohash.org/%s.png?bgset=bg2`

// RobohashSeedForString returns a hashed seed value for a string.
func RobohashSeedForString(s string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(s)))
}

// RobohashURLForSeed returns a robohash url for a seed.
func RobohashURLForSeed(s string) string {
	return fmt.Sprintf(roboHashURLTemplate, s)
}
