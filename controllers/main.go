package controllers

import "net/http"

type ErrorResponse struct {
	Message string `json:"message"`
}

func MainHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}
