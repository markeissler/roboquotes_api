package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/markeissler/Roboquotes_API/utils"
)

func FetchRandomQuote(w http.ResponseWriter, r *http.Request) {
	quote, err := utils.QuotesOnDesignGetRandom()
	if err != nil {
		log.Println(err)
		err := ErrorResponse{
			Message: fmt.Sprintf("unable to fetch quote: %v", err.Error()),
		}
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(quote)
}
