package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/markeissler/Roboquotes_API/models"
	"bitbucket.org/markeissler/Roboquotes_API/utils"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

var db *gorm.DB

func init() {
	var err error
	if db, err = utils.ConnectDB(); err != nil {
		log.Fatal(err)
	}
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	user := new(models.User)

	if err := json.NewDecoder(r.Body).Decode(user); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("unable to read content: %v", err.Error())))
		return
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("password encryption failed: %v", err.Error())))
		return
	}

	user.Password = string(pass)

	// set robohash and retrieve url only if email isn't blank
	if user.Email != "" {
		user.RobohashSeed = utils.RobohashSeedForString(user.Email)
		user.RobohashUrl = utils.RobohashURLForSeed(user.RobohashSeed)
	} else {
		log.Println("unable to create robohash seed, email is empty")
	}

	dbResult := db.Create(user)
	if dbResult.Error != nil {
		log.Println(dbResult.Error)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("user creation failed: %v", dbResult.Error)))
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(user)
}

func FetchUser(w http.ResponseWriter, r *http.Request) {
	user := new(models.User)
	params := mux.Vars(r)

	id := params["id"]
	dbResult := db.First(user, id)
	if dbResult.Error != nil {
		log.Println(dbResult.Error)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("user fetch failed: %v", dbResult.Error)))
		return
	}

	// set robohash url only if robohash_seed isn't blank
	if user.RobohashSeed != "" {
		user.RobohashUrl = utils.RobohashURLForSeed(user.RobohashSeed)
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(user)
}
