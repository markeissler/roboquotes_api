package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/markeissler/Roboquotes_API/models"
)

func Login(w http.ResponseWriter, r *http.Request) {
	user := new(models.User)

	if err := json.NewDecoder(r.Body).Decode(user); err != nil {
		log.Println(err)
		err := ErrorResponse{
			Message: fmt.Sprintf("unable to read content: %v", err.Error()),
		}
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}

	response, err := user.Authenticate(db, user.Email, user.Password)
	if err != nil {
		err := ErrorResponse{
			Message: fmt.Sprintf("authentication error: %v", err.Error()),
		}
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)
}
