package main

import (
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/joho/godotenv"

	"bitbucket.org/markeissler/Roboquotes_API/routes"
	"github.com/gorilla/handlers"
	"github.com/jinzhu/gorm"
)

var DB *gorm.DB

func main() {
	// load env vars
	if err := godotenv.Load(); err != nil {
		log.Fatal("error loading env vars")
	}

	// register routes
	router := routes.Register()

	// wrap all handlers with logging handler
	loggedRouter := handlers.LoggingHandler(os.Stdout, router)

	serverAddress := os.Getenv("serverAddress")
	serverPort := os.Getenv("serverPort")

	server := &http.Server{
		Handler:      loggedRouter,
		Addr:         strings.Join([]string{serverAddress, serverPort}, ":"),
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
	}

	log.Println("Server starting on port", serverPort)

	log.Fatal(server.ListenAndServe())
}
